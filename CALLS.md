## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Datadog. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Datadog.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Datadog. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create a user for your organization.

### ARGUMENTS

* **`handle`** [*required*]: The user handle, must be a valid email.
* **`name`** [*optional*, *default*=**None**]: The name of the user.
* **`access_role`** [*optional*, *default*=**st**]: The access role of the user. Choose from:
    *  **st** (standard user),
    *  **adm** (admin user),
    *  **ro** (read-only user).
    *Note: users can be created with admin access role only with application keys belonging to administrators...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsers(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get all user for your organization.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a user details.

### Arguments

* **`user_handle`** [*required*]: The handle of the user.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update a user informations,

**Note**: It can only be used with application keys belonging to administrators.

##### ARGUMENTS
* **`id`** [*required*]: The handle of the user.
* **`name`** [*optional*, *default*=**None**]: The new name of the user.
* **`email`** [*optional*, *default*=**None**]: The new email of the user.
* **`disabled`** [*optional*, *default*=**None**]: The new disabled status of the user.
* **`access_role`** [*optional*, *default*=**st**]: The access role of the...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableUser(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a user from an organization.

**Note**: This endpoint can only be used with application keys belonging to administrators.

### Arguments

* **`id`** [*required*]: The handle of the user.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationCheck(apiKey, callback)</td>
    <td style="padding:15px">All requests to Datadog's API must be authenticated. Requests that write data require *reporting access* and require an `API key`. Requests that read data require *full access* and also require an `application key`.

[Manage your account's API and application keys](https://app.datadoghq.com/account/settings#api).

This GET call just checks to see if an API call key is valid.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postACheckStatus(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This POST call allows for you to POST check statuses for use with monitors

### Arguments

* **`check`** *[required]*: The text for the message

* **`host_name`** *[required]*: The name of the host submitting the check

* **`status`** *[optional]*: An integer for the status of the check:
    * 0 : OK
    * 1 : WARNING
    * 2 : CRITICAL
    * 3 : UNKNOWN


* **`timestamp`** *[optional]*: POSIX timestamp of the event.

* **`message`** *[optional]*:   A description of why this stat...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replyToComment(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This POST call allows for you to POST status messages to a previous post via ID.

### Arguments

* **`message`** [*required*]: The comment text.

* **`handle`** [*optional*, *default* = **application key owner**]: The handle of the user making the comment.

* **`related_event_id`** [*optional*, *default* = **None**]: The id of another comment or event to reply to.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAComment(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This POST call allows for you to change a previously posted comment by commentId

### Arguments

* **`message`** [*optional*, *default* = **original message**]: The comment text.

* **`handle`** [*optional*, *default* = **Application key owner**]: The handle of the user making the comment.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAComment(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Thiscall allows for you to DELETE a previously posted comment by with a given  .

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createADashboard(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create a Dashboard in Datadog.

### Arguments

* **`title`** [*required*]: Title of the dashboard.
* **`widgets`** [*required*]: List of widgets to display on the dashboard. Widget definitions follow this form:
    * **`definition`** [*required*]: [Definition of the widget.](https://docs.datadoghq.com//graphing/widgets)
    * **`id`** [*optional*, *default*=**auto-generated integer**]: ID of the widget.
* **`layout_type`** [*required*]: Layout type of the dashboard. Available value...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateADashboard(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update a Dashboard in Datadog.

### Arguments

* **`title`** [*required*]: Title of the dashboard.
* **`widgets`** [*required*]: List of widgets to display on the dashboard. Widget definitions follow this form:
    * **`definition`** [*required*]: [Definition of the widget.](https://docs.datadoghq.com/graphing/widgets)
    * **`id`** [*optional*, *default*=**auto-generated integer**]: ID of the widget.
* **`layout_type`** [*required*]: Layout type of the dashboard. Available values...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADashboard(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete an existing dashboard.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADashboard(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Fetch an existing dashboard's definition.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDashboards(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Fetch all existing Dashboards.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADashboardList(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Fetch an existing dashboard list’s definition.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateADashboardList(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update the name of a dashboard list

### Arguments

*   **`name`** [*required*]: The name of the dashboard list.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADashboardList(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete an existing dashboard list.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDashboardLists(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Fetch all of your existing dashboard list definitions.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createADashboardList(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create an empty dashboard list.

### Arguments

*   **`name`** [*required*]: The name of the dashboard list.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemsOfADashboardList(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Fetch the dashboard list’s dashboard definitions.

### Arguments

This end point takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addItemsToDashboardList(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Add dashboards to an existing dashboard list.

### Arguments

*   **`dashboards`** [*required*]: A list of dashboards to add to the list. Dashboard definitions follow this form:
    *   **`type`** [*required*]: The type of the dashboard. The type must be one of:
        * `"custom_timeboard"`
        * `"custom_screenboard"`
        * `"integration_screenboard"`
        * `"integration_timeboard"`
        * `"host_timeboard"`
    *   **`id`** [*required*]: The id of the dashboard.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateItemsOfADashboardList(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update dashboards of an existing dashboard list.

### ARGUMENTS

*   **`dashboards`** [*required*]: The new list of dashboards for the dashboard list. Dashboard definitions follow this form:
    *   **`type`** [*required*]: The type of the dashboard. The type must be one of:
        * `"custom_timeboard"`
        * `"custom_screenboard"`
        * `"integration_screenboard"`
        * `"integration_timeboard"`
        * `"host_timeboard"`
    *   **`id`** [*required*]: The id of th...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteItemsFromADashboardList(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete dashboards from an existing dashboard list.

### Arguments

*   **`dashboards`** [*required*]: A list of dashboards to remove from the list. Dashboard definitions follow this form:
    *   **`type`** [*required*]: The type of the dashboard. The type must be one of:
        * `"custom_timeboard"`
        * `"custom_screenboard"`
        * `"integration_screenboard"`
        * `"integration_timeboard"`
        * `"host_timeboard"`
    *   **`id`** [*required*]: The id of the d...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleMonitorDowntime(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Schedule a single Downtime.

### ARGUMENTS

* **`scope`** [*required*]: The scope(s) to which the downtime applies, e.g. `host:app2`. Provide multiple scopes as a comma-separated list, e.g. `env:dev,env:prod`. The resulting downtime applies to sources that matches ALL provided scopes (i.e. `env:dev` **AND** `env:prod`), NOT any of them.
* **`monitor_tags`** [*optional*, *default*=**no monitor tag filter**]: A comma-separated list of monitor tags, i.e. tags that are applied directly...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMointorDowntimes(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get All Scheduled Downtimes

### ARGUMENTS

* **`current_only`** [*optional*, *default* = **False**]: Only return downtimes that are activewhen the request is made.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitorDowntime(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update a single Downtime by downtime_id.

### Arguments

* **`id`** [*required*]: The integer id of the downtime to be updated
* **`scope`** [*required*]: The scope to which the downtime applies, e.g. 'host:app2'. Provide multiple scopes as a comma-separated list, e.g. 'env:dev,env:prod'. The resulting downtime applies to sources that matches ALL provided scopes (i.e. env:dev AND env:prod), NOT any of them.
* **`monitor_tags`** [*optional*, *default*=**no monitor tag filter**]: A c...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelMonitorDowntime(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">DELETE a Downtime Detail by downtime_id</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMonitorDowntime(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get Downtime Detail by downtime_id

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelMonitorDowntimesByScope(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

DELETE all Downtimes that match the scope of X

### Arguments

* **`scope`** [*required*]:   Cancel all downtimes with the given scope(s), e.g.: `env:prod`, `role:db,role:db-slave`</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEmbeddableGraphs(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Gets a list of previously created embeddable graphs.

Returns: A JSON list containing information on previously created embeds from both the UI and the API. Each JSON graph response is in the same format as returned by GET `api/v1/graph/embed/ `.

### ARGUMENTS

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEmbedGraph(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Creates a new embeddable graph.

Returns: A JSON consisting of the same elements returned by GET `api/v1/graph/embed/ `. On failure, the return value is a JSON containing an error message {errors: [messages]}.

Note: If an embed already exists for the exact same query in a given organization, the older embed is returned instead of creating a new embed.

If you are interested in using template variables, see [Embeddable Graphs with Template Variables](https://docs.datadoghq.com/grap...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASpecificEmbed(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get the HTML fragment for a previously generated embed with embed_id.

Returns: A JSON object with 8 elements:

* `embed_id`: Token of the embed
* `graph_title`: Tile of the graph
* `dash_name`: Name of the dashboard the graph is on (null if none)
* `dash_url`: URL of the dashboard the graph is on (null if none)
* `shared_by`: ID of the use who shared the embed
* `html`: HTML fragment for the embed (iframe)
* `revoked`: Boolean flag for whther or not the embed is revoked

On failur...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableEmbed(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Enable a specified embed.

Returns: A JSON containing the success message `{success: [message]}`. On failure, the return value is a JSON containing an error message `{errors: [messages]}`.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeEmbed(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Revoke a specified embed.

Returns: A JSON containing the success message `{success: [message]}`. On failure, the return value is a JSON containing an error message `{errors: [messages]}`.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAnEvent(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows you to post events to the stream. Tag them, set priority and event aggregate them with other events.

### Arguments

* **`title`** [*required*]:   The event title. *Limited to 100 characters.* Use `msg_title` with [the Datadog Ruby library](https://github.com/DataDog/dogapi-rb).
* **`text`** [*required*]:   The body of the event. *Limited to 4000 characters.* The text supports [markdown](https://docs.datadoghq.com/graphing/event_stream/#markdown-events). Use `m...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryTheEventStream(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

The [event stream](https://docs.datadoghq.com/graphing/event_stream) can be queried and filtered by time, priority, sources and tags.
Note: if the event you're querying contains markdown formatting of any kind, you may see characters such as %,\,n in your output

### Arguments

* **`start`** [*required*]: POSIX timestamp.
* **`end`** [*required*]: POSIX timestamp.
* **`priority`** [*optional*, *default*=**None**]: Priority of your events: **low** or **normal**.
* **`sources`** [*op...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnEvent(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get event details by event_id

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnEvent(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

DELETE event details by event_id

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">graphASnapshot(apiKey, applicationKey, metricQuery, start, end, title, callback)</td>
    <td style="padding:15px">### Overview

Get a Graph snapshot.

##### ARGUMENTS

* **`metric_query`** [*required*]: The metric query.
* **`start`** [*required*]: The POSIX timestamp of the start of the query.
* **`end`** [*required*]: The POSIX timestamp of the end of the query.
* **`event_query`** [*optional*, *default*=**None**]: A query that adds event bands to the graph.
* **`graph_def`** [*optional*, *default*=**None**]: A JSON document defining the graph. `graph_def` can be used instead of `metric_query`. The JSON d...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchHosts(apiKey, applicationKey, filter, sortField, sortDir, start, count, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows searching for hosts by name, alias, or tag. Hosts live within the past 3 hours are included. Results are paginated with a max of 100 results at a time.

### Arguments

* **`filter`** [*optional*, *default*=**None**]: Query string to filter search results.
* **`sort_field`** [*optional*, *default*=**cpu**]: Sort hosts by the given field. Options: **status**, **apps**, **cpu**, **iowait**, **load**
* **`sort_dir`** [*optional*, *default*=**desc**]: Direction of s...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostTotals(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint returns the total number of active and up hosts in your Datadog account. Active means the host has reported in the past hour, and up means it has reported in the past two hours.

### ARGUMENTS

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">muteAHost(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Mute a host in order to avoid any monitor to trigger on it.

### Arguments

* **`end`** [*optional*, *default*=**None**]: POSIX timestamp when the host is unmuted. If omitted, the host remains muted until explicitly unmuted.
* **`message`** [*optional*, *default*=**None**]: Message to associate with the muting of this host.
* **`override`** [*optional*, *default*=**False**]: If true and the host is already muted, replaces existing host mute settings.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmuteAHost(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Unmute a previously muted host

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAWSAccounts(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List all configured AWS accounts for Datadog.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnAWSIntegration(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create a Datadog-Amazon Web Services integration.

**Note**: Using the `POST` method updates your integration configuration by **adding** your new configuration to the existing one in your Datadog organization.

### Arguments

* **`account_id`** [*required*]:

    Your AWS Account ID without dashes.
    [Consult the Datadog AWS integration to learn more][1] about your AWS account ID.

* **`access_key_id`** [*optional*, *default*=**None**]:

    If your AWS account is a GovCloud or ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAWSIntegration(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete your Datadog-AWS integration directly through Datadog API. [Read more about Datadog-AWS integration][1].

### Arguments

This endpoint takes no JSON argument.

[1]: https://docs.datadoghq.com/integrations/amazon_web_services</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAvailableNamespaceRules(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List available namespace rules for your AWS integration

### Arguments

This endpoint takes no JSON arguments</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAWSFilteringRules(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List AWS Accounts (role-based only) in Datadog.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureAWSFilteringRuleCopy(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List AWS Accounts (role-based only) in Datadog. [Read more about Datadog-AWS integration][1].

### Arguments

* **`account_id`** [*required*]: Your AWS Account ID without dashes. [Consult the Datadog AWS integration to learn more][2] about your AWS account ID.

* **`access_key_id`** [*optional*, *default*=**None**]: If your AWS account is a GovCloud or China account, enter the corresponding Access Key ID.$

* **`namespace`** [*required*]: The AWS namespace to apply filtering too (e...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSFilteringRule(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">Delete a Datadog-AWS Filtering rule</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateNewExternalIDs(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Generate a new AWS external id for a given AWS account id and role name pair.

### Arguments


* **`account_id`** [*required*]:

    Your AWS Account ID without dashes.
    [Consult the Datadog AWS integration to learn more][1] about your AWS account ID.

* **`role_name`** [*required*]:

    Your Datadog role delegation name.
    For more information about you AWS account Role name, [see the Datadog AWS integration configuration info][2].

[1]: /integrations/amazon_web_services/#co...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureAccount(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List all Datadog-Azure integrations configured for your Datadog Account.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAzureAccountSHostFilters(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update Datadog-Azure host filtering.

### Arguments

Refer to the [Datadog-Azure integration installation instructions][1] to see how to obtain the following field values for your organization.

* **`tenant_name`** [*required*]: Your Azure Active Directory ID.

* **`client_id`** [*required*]: Your Azure web application ID.

* **`client_secret`** [*required*]: Your Azure web application secret key.

* **`host_filters`** [*optional*, *default*=**None**]: Limit the Azure instances th...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAzureAccount(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a Datadog-Azure integration.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGCPAccounts(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List all Datadog-GCP integrations configured for your Datadog Account.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGCPAccount(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Configure your Datadog-Google Cloud Platform integration directly through the Datadog API.
[Read more about Datadog-Google Cloud Platform integration][1].

**Note**:  Using the `POST` method updates your integration configuration by **adding** your new configuration to the existing one in your Datadog organization.

### Arguments

All of the following fields' values are provided by the JSON service account key file created in the [GCP Console for service accounts][2]; Refer to t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAGCPServiceAccountSAutomuteOption(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update your Datadog-Google Cloud Platform integration Automute options.
[Read more about Datadog-Google Cloud Platform integration][1].

### Arguments

* **`project_id`** [*required*]: Your Google Cloud project ID found in your JSON service account key.

* **`client_email`** [*required*]: Your email found in your JSON service account key.

* **`automute`** [*optional*, *default*=**false**]: Silence monitors for expected GCE instance shutdowns.

[1]: https://docs.datadoghq.com/integ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAGCPServiceAccount(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete the Datadog-Google Cloud Platform integration. [Read more about Datadog-Google Cloud Platform integration][1].

### Arguments

This endpoint takes no JSON argument.

[1]: https://docs.datadoghq.com/integrations/google_cloud_platform</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPagerDutyConfiguration(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List all Datadog-PagerDuty integrations configured for your Datadog Account.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNewServicesAndSchedules(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Add new services and schedules to your Datadog-PagerDuty integration. [Read more about Datadog-PagerDuty integration][1].

### Arguments

* **`services`** : Array of PagerDuty service objects. [Learn how to configure you Datadog service with PagerDuty documentation][2]. A PagerDuty service object is composed by:
    * **`service_name`** [*required*]: Your Service name in PagerDuty.
	* **`service_key`** [*required*]: Your Service name associated service key in Pagerduty.
* **`schedu...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePagerDutyConfiguration(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a Datadog-PagerDuty integration configured in your Datadog Account. [Read more about Datadog-PagerDuty integration][1].

### Arguments

This endpoint takes no JSON arguments


[1]: https://docs.datadoghq.com/integrations/pagerduty</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlackConfigurationDetails(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List all Datadog-Slack integrations configured for your Datadog Account.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addChannelsToExistingConfiguration(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Add channels to your existing Datadog-Slack integration directly through Datadog API. [Read more about Datadog-Slack integration][1].

### Arguments

* **`channels`** [*required*]: Array of slack channel objects to post to. A slack channel object is composed by:

    * **`channel_name`** [*required*]: Your channel name e.g: `#general`, `#private`

    * **`transfer_all_user_comments`** [*optional*, *default*=**False**]: To be notified for every comment on a graph, set it to `true`....(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSlackConfigurationDetails(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a Datadog-Slack integration configured in your Datadog Account. [Read more about Datadog-Slack integration][1].

### Arguments

This endpoint takes no JSON arguments


[1]: https://docs.datadoghq.com/integrations/slack</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllWebhookConfigurations(apiKey, applicationKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addToCurrentConfiguration(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">ARGUMENTS</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebhookConfigurations(apiKey, applicationKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCurrentConfiguration(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">ARGUMENTS</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAPIKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get all API keys available for your account.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createANewAPIKey(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create an API key with a given name.

### Arguments

* **`name`** [*required*]: Name of your API key.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAGivenAPIKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a given API key.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAGivenAPIKey(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Edit an API key name.

### Arguments

* **`name`** [*required*]: Name of your API key.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAPIKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a given API key.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get all application keys available for your account.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createANewApplicationKey(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create an application key with a given name.

### Arguments

* **`name`** [*required*]: Name of your application key.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAGivenApplicationKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a given application key.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAGivenApplicationKey(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Edit an application key name.

### Arguments

* **`name`** [*required*]: Name of your application key.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnApplicationKey(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a given application key.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendLogsOverHTTP(body, ddsource, service, hostname, datadogApiKey, callback)</td>
    <td style="padding:15px">### Overview

Send your logs to your Datadog platform over HTTP. Limits per HTTP request are:

* Maximum content size per payload: 2MB
* Maximum size for a single log: 256kB
* Maximum array size if sending multiple logs in an array: 50 entries

**Note**: If you are in the Datadog EU site (`app.datadoghq.eu`), the HTTP log endpoint is: `http-intake.logs.datadoghq.eu`.


### Arguments

| Item             | Description                                                                                 ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAListOfLogs(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

List endpoint returns logs that match a log search query. Results are paginated.

### Arguments

* **`query`** [*required*]: The search query - following the [Log search syntax][1] .
* **`time.from`** [*required*]: Minimum timestamp for requested logs. Format can be either
    - an ISO-8601 string
    - a unix timestamp (number representing the elapsed millisec since epoch)
    - a relative time (`now -10m`, `now - 1h`, `now - 1d`)
* **`time.to`** [*required*]: Maximum timestamp fo...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllActiveMetrics(apiKey, applicationKey, from, host, callback)</td>
    <td style="padding:15px">### Overview

Get the list of actively reporting metrics from a given time until now. This endpoint is not available in the Python and Ruby libraries.

### Arguments

* **`from`** [*required*]: EPOCH timestamp from which to list metrics.
* **`host`** [*optional*]: Hostname for filtering the list of metrics returned. If set, metrics retrieved are those with the corresponding hostname tag.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendTimeSeriesPoint(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

The metrics end-point allows you to post time-series data that can be graphed on Datadog's dashboards. The limit for compressed payloads is 3.2 megabytes (3200000), and 62 megabytes (62914560) for decompressed payloads.

### Arguments

* **`series`** [*required*]: Pass a JSON array where each item in the array contains the following arguments:

    * **`metric`** [*required*]: The name of the timeseries
    * **`type`** [*optional*, *default*=**gauge**]: [Type][1] of your metric ei...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryTimeSeriesPoints(apiKey, applicationKey, from, to, query, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows you to query for metrics from any time period.

### Arguments

* **`from`** [*required*]: EPOCH Timestamp from which to query metrics data.
* **`to`** [*required*]: EPOCH Timestamp until which to query metrics data.
* **`query`** [*required*]: The query string for the metric to retrieve. See [graphing documentation]() to learn how to build a metrics query.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewMetricMetadata(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

The metrics metadata endpoint allows you to get metadata about a specific metric.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editMetricMetadata(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

The metrics metadata endpoint allows you to edit fields of a metric's metadata. [Find more about supported types][1]

### Arguments

* **`type`** [*required*]: [Metric type][1] such as **gauge** or **rate**
* **`description`** [*optional*, *default*=**None**]: String description of the metric" default
* **`short_name`** [*required*]: Short name string of the metric
* **`unit`** [*optional*, *default*=**None**]: Primary unit of the metric such as **byte** or **operation**. [See the ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchMetrics(q, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows you to search for metrics from the last 24 hours in Datadog.

### Arguments

* `q` [*required*]: The query string. Must be prefixed with `metrics:`.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAMonitor(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

If you manage and deploy monitors programmatically, it's easier to define the monitor in the Datadog UI and [export its valid JSON][1].

### Arguments

*   **`type`** [*required*]: The [type of the monitor][2], chosen from:

| Monitor Type | type attribute value |
| :--------    | :-------             |
| anomaly      | `query alert`        |
| apm          | `query alert`        |
| composite    | `composite`          |
| custom       | `service check`      |
| event        | `eve...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMonitorSDetails(apiKey, applicationKey, groupStates, callback)</td>
    <td style="padding:15px">### Overview

Get a monitor details based on its monitor ID.

### Arguments

* **`group_states`** [*optional*, *default*=**None**]: If this argument is set, the returned data includes additional information (if available) regarding the specified group states, including the last notification timestamp, last resolution timestamp and details about the last time the monitor was triggered. The argument should include a string list indicating what, if any, group states to include. Choose one or more f...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAMonitorSDetails(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Edit a monitor details based on its monitor ID.

### Arguments

* **`query`** [*required*]: The metric query to alert on.
* **`name`** [*required*]: The name of the monitor.
* **`message`** [*optional*, *default*=**dynamic, based on query**]: A message to include with notifications for this monitor. Email notifications can be sent to specific users by using the same '@username' notation as events.
* **`options`** [*optional*, *default*=**{}**]: Refer to the create monitor documenta...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAMonitor(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete a monitor based on its monitor ID.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resolveMonitor(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Resolve a monitor by monitorId & Group.

### Arguments

* **`resolve`** [*required*]: Array of group(s) to resolve for a given monitor_id, e.g.: `{"monitor_id": "group_to_resolve"}`

    * It supports multiple groups per monitor, e.g.: `resolve: [{"monitor_id": "group_1"}, {"monitor_id": "group_2"}]`

    * It can also resolve all triggered groups with the pseudo-group `ALL_GROUPS`: `resolve: [{"monitor_id": "ALL_GROUPS"}]`</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">muteASpecificMonitor(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Mute a specific monitor base on its ID.

### Arguments
* **`scope`** [*optional*, *default*=**None**]: The scope to apply the mute to, e.g. **role:db**. For example, if your alert is grouped by `{host}`, you might mute `host:app1`.
* **`end`** [*optional*, *default*=**None**]: A POSIX timestamp for when the mute should end.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmuteASpecificMonitor(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

### Arguments

* **`scope`** [*optional*, *default*=**None**]: The scope to apply the mute to. For example, if your alert is grouped by {host}, you might mute 'host:app1'
* **`all_scopes`** [*optional*, *default* = **False**]: Clear muting across all scopes</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">muteAllMonitors(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Muting prevents all monitors from notifying through email and posts to the [event stream][1]. State changes are only visible by checking the alert page.

### Arguments

This endpoint takes no JSON arguments.

[1]: https://docs.datadoghq.com/graphing/event_stream</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmuteAllMonitors(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Disables muting all monitors. Throws an error if mute all was not enabled previously.

### Arguments

This endpoint takes no JSON arguments.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateAMonitorDefinitions(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Validates a monitor definition.

### Arguments

See [Create a monitor][1] documentation for details on constructing a monitor definition.

[1]: https://docs.datadoghq.com/api/#create-a-monitor</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorSearch(apiKey, applicationKey, query, page, perPage, sort, callback)</td>
    <td style="padding:15px">### Overview

Search and filter your monitors details.

### Arguments

* **`query`** [*optional*]: After entering a search query in your [Manage Monitor page][1] use the query parameter value in the URL of the page as value for this parameter. Consult the dedicated [manage monitor documentation][2] page to learn more. The query can contain any number of space-separated monitor attributes, for instance `query="type:metric status:alert"`.

* **`page`** [*optional*, *default* = **0**]: Page to star...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorGroupSearch(apiKey, applicationKey, query, page, perPage, sort, callback)</td>
    <td style="padding:15px">### Overview

Search and filter your monitor groups details.

### ARGUMENTS

* **`query`** [*optional*]: After entering a search query in your [Manage Monitor page][1] use the query parameter value in the URL of the page as value for this parameter. Consult the dedicated [manage monitor documentation][2] page to learn more.

* **`page`** [*optional*, *default* = **0**]: Page to start paginating from.

* **`per_page`** [*optional*, *default*=**30**]: Number of monitors to return per page.

* **`s...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChildOrganization(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint requires the [multi-org account][1] feature and must be enabled by [contacting support][2].

### Arguments

* **`name`** [*required*]: The name of the new child-organization, limited to 32 characters.
* **`subscription`** [*required*]: A JSON array of subscription type. Types available are `trial`, `free`, and `pro`.
* **`billing`** [*required*]: A JSON array of billing type. Note that only `parent_billing` is supported.

Once a new child-organization is created, you ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOrganizations(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get all child-organisation details for a given Master organisation.

### Arguments

This endpoint takes no JSON argument.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganization(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview
Get an organisation details based on its public ID.

### Arguments

* **`public_id`** [*required*]: The public id of the organization.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganization(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Update a Datadog organization information.

##### ARGUMENTS
* **`name`** [*optional*]: The organization name.
* **`settings`** [*optional*]: A JSON array of settings. Settings include:
    * **`saml`** - Set the boolean property `enabled` to enable or disable single sign on with SAML. See the [SAML documentation][1] for more information about all SAML settings.
    * **`saml_idp_initiated_login`** - has one property `enabled` (boolean).v
    * **`saml_strict_mode`** - has one prope...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadIDPMetadata(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

There are a couple of options for updating the Identity Provider (IdP) metadata from your SAML IdP.

* **Multipart Form-Data**: Post the IdP metadata file using a form post.
* **XML Body**: Post the IdP metadata file as the body of the request.

### Multipart Form-Data

#### Headers

* **`Content-Type: multipart/form-data`**

#### Arguments

* **`idp_file`** [*required*]: The path to the XML metadata file you wish to upload.

### XML Body

#### Headers
* **`Content-Type: applicatio...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createATest(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Create a Synthetics test to initiate and configure the tests you want Datadog to send to your API endpoints or to your browser app. You can configure the endpoints being tested, the number of tests, and where they are coming from. The parameters required are different for API and browser tests and they are marked accordingly—if a parameter is marked as _required_, it is required for both types of tests. Once you create a test, it shows up in the UI in your [Synthetics list][1]

A b...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTests(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a list of all existing tests.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startOrPauseATest(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Start or pause an existing Synthetics test.

##### Arguments

*   **`new_status`** - _required_ - A key-value pair where you define whether you want to start or pause a test. Valid values are `live` and `paused`. Returns `true` if the status has been changed, and `false` if the status has stayed the same.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editATest(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Use this method to update an existing Synthetics test. In order to update a test, you have to submit the same payload as creating a test.

The parameters required are different for API and browser tests and they are marked accordingly—if a parameter is marked as _required_, it is required for both types of tests.

A browser test is treated like a GET API test. This method gives you the ability to update the browser test, but you have to use the UI to [record your test][1].

In orde...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteATest(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Delete one or many tests.

### Arguments

*   **`public_ids`** - _required_ - A JSON list of the ID or IDs of the tests that you want to delete.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTestResults(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get the most recent results for a specific test.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASpecificTestResult(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a specific result for a specific test.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getATest(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get information on a specific test.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesForBrowserChecks(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a list of devices for browser checks.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableLocations(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Get a list of available locations

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Return a mapping of tags to hosts for your whole infrastructure.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostTags(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Return the list of tags that apply to a given host.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTagsToAHost(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows you to add new tags to a host, optionally specifying where these tags come from.

### Arguments

* **`tags`** [*required*]: A list of tags to apply to the host
* **`source`** [*optional*, *default*=**users**]: The source of the tags (e.g. chef, puppet). [Complete list of source attribute values][1]

[1]: https://docs.datadoghq.com/integrations/faq/list-of-api-source-attribute-value</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostTags(body, apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

This endpoint allows you to update/replace all tags in an integration source with those supplied in the request.

### Arguments

* **`tags`** [*required*]: A list of tags
* **`source`** [*optional*, *default*=**users**]: The source of the tags (e.g. chef, puppet). [Complete list of source attribute values][1]

[1]: https://docs.datadoghq.com/integrations/faq/list-of-api-source-attribute-value</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeHostTags(apiKey, applicationKey, callback)</td>
    <td style="padding:15px">### Overview

Remove all tags in an integration source for a single host.

### Arguments

*This endpoint takes no JSON arguments.*</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendTraces(body, callback)</td>
    <td style="padding:15px">### Overview

Datadog's APM allows you to collect performance metrics by tracing your code to determine which parts of your application are slow or inefficient.

Tracing data is sent to the Datadog Agent via an HTTP API. We provide some [official libraries][1] that simplify sending metrics to the Datadog Agent, however you may want to interact directly with the API to instrument applications that cannot use the libraries or are written in languages that don't yet have an official Datadog Tracing...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendServices(body, callback)</td>
    <td style="padding:15px">https://docs.datadoghq.com/api/?lang=bash#send-traces</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostsAndContainersHourlyUsage(apiKey, applicationKey, startHr, endHr, callback)</td>
    <td style="padding:15px">### Overview

Get Hourly Usage For Hosts and Containers.

### Arguments

* **`start_hr`** [*required*]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage beginning at this hour
* **`end_hr`** [*optional*, *default*=**1d+start_hr**]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage ending BEFORE this hour

#### Response

* **`container_count`**: Shows the total number of containers reporting via the Docker integration during the hour.
* **`ho...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogsHourlyUsage(apiKey, applicationKey, startHr, endHr, callback)</td>
    <td style="padding:15px">### Overview

Get Hourly Usage For Logs.

### Arguments
* **`start_hr`** [*required*]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage beginning at this hour
* **`end_hr`** [*optional*, *default*=**1d+start_hr**]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage ending BEFORE this hour

#### Response

* **`ingested_events_bytes`**: Contains the number of log bytes ingested.
* **`indexed_events_count`**: Contains the number of log events in...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomMetricsHourlyUsage(apiKey, applicationKey, startHr, endHr, callback)</td>
    <td style="padding:15px">### Overview

Get Hourly Usage For [Custom Metrics][1].

### Arguments

* **`start_hr`** [*required*]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage beginning at this hour
* **`end_hr`** [*optional*, *default*=**1d+start_hr**]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage ending BEFORE this hour

[1]: https://docs.datadoghq.com/developers/metrics/custom_metrics</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTop500CustomMetricsByHourlyAverage(apiKey, applicationKey, month, names, callback)</td>
    <td style="padding:15px">### Overview

Get Top [Custom Metrics][1] By Hourly Average.

### Arguments
* **`month`** [*required*]: Datetime in ISO-8601 format, UTC, precise to month: [YYYY-MM] for usage beginning at this hour.
* **`names`** [*optional*, *default*=**None**]: Comma-separated list of metric names.

[1]: https://docs.datadoghq.com/developers/metrics/custom_metrics</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultiOrgUsageDetails(apiKey, applicationKey, startMonth, endMonth, includeOrgIds, callback)</td>
    <td style="padding:15px">### Overview

Get usage across your multi-org account

### Arguments

* **`start_month`** [*required*]: Datetime in ISO-8601 format, UTC, precise to month: [YYYY-MM] for usage beginning in this month. Maximum of 15 months ago.
* **`end_month`** [*optional*, *default*=**current_month-3d**]: Datetime in ISO-8601 format, UTC, precise to month: [YYYY-MM] for usage ending this month.
* **`include_org_details`** [*optional*, *default*=**true**]: Include usage summaries for each sub-org.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceSearchHourlyUsage(apiKey, applicationKey, startHr, endHr, callback)</td>
    <td style="padding:15px">### Overview

Get hourly usage For Trace Search.

### Arguments
* **`start_hr`** [*required*]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage beginning at this hour
* **`end_hr`** [*optional*, *default*=**1d+start_hr**]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage ending BEFORE this hour

#### Response

* **`indexed_events_count`**: Contains the number of Trace Search events indexed.
* **`hour`**: The hour for the usage.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFargateHourlyUsage(apiKey, applicationKey, startHr, endHr, callback)</td>
    <td style="padding:15px">### Overview

Get Hourly Usage For [Fargate][1].

### Arguments

* **`start_hr`** [*required*]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage beginning at this hour
* **`end_hr`** [*optional*, *default*=**1d+start_hr**]: Datetime in ISO-8601 format, UTC, precise to hour: [YYYY-MM-DDThh] for usage ending BEFORE this hour

[1]: https://docs.datadoghq.com/integrations/ecs_fargate</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
