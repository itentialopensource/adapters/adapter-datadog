
## 0.6.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!7

---

## 0.5.4 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!6

---

## 0.5.3 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!5

---

## 0.5.2 [01-16-2020] & 0.5.1 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!4

---

## 0.5.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!3

---

## 0.4.0 [09-12-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorize and prepare for app artifact

See merge request itentialopensource/adapters/telemetry-analytics/adapter-datadog!1

---

## 0.1.1 [06-18-2019]

- Initial Commit

See commit 88ea544

---
