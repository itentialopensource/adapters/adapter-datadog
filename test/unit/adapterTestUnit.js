/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-datadog',
      type: 'Datadog',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Datadog = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Datadog Adapter Test', () => {
  describe('Datadog Class Tests', () => {
    const a = new Datadog(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('datadog'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('datadog'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Datadog', pronghornDotJson.export);
          assert.equal('Datadog', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-datadog', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('datadog'));
          assert.equal('Datadog', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-datadog', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-datadog', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createUser - errors', () => {
      it('should have a createUser function', (done) => {
        try {
          assert.equal(true, typeof a.createUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUser(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsers - errors', () => {
      it('should have a getAllUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllUsers(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllUsers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getUser(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUser(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableUser - errors', () => {
      it('should have a disableUser function', (done) => {
        try {
          assert.equal(true, typeof a.disableUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.disableUser(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-disableUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.disableUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-disableUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationCheck - errors', () => {
      it('should have a authenticationCheck function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.authenticationCheck(null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-authenticationCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postACheckStatus - errors', () => {
      it('should have a postACheckStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postACheckStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postACheckStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postACheckStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.postACheckStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postACheckStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.postACheckStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postACheckStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replyToComment - errors', () => {
      it('should have a replyToComment function', (done) => {
        try {
          assert.equal(true, typeof a.replyToComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replyToComment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replyToComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.replyToComment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replyToComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.replyToComment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replyToComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAComment - errors', () => {
      it('should have a editAComment function', (done) => {
        try {
          assert.equal(true, typeof a.editAComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAComment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editAComment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editAComment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAComment - errors', () => {
      it('should have a deleteAComment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAComment(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAComment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createADashboard - errors', () => {
      it('should have a createADashboard function', (done) => {
        try {
          assert.equal(true, typeof a.createADashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createADashboard(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createADashboard('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createADashboard('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateADashboard - errors', () => {
      it('should have a updateADashboard function', (done) => {
        try {
          assert.equal(true, typeof a.updateADashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateADashboard(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateADashboard('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateADashboard('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteADashboard - errors', () => {
      it('should have a deleteADashboard function', (done) => {
        try {
          assert.equal(true, typeof a.deleteADashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteADashboard(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteADashboard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getADashboard - errors', () => {
      it('should have a getADashboard function', (done) => {
        try {
          assert.equal(true, typeof a.getADashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getADashboard(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getADashboard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getADashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDashboards - errors', () => {
      it('should have a getAllDashboards function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDashboards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllDashboards(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllDashboards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllDashboards('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllDashboards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getADashboardList - errors', () => {
      it('should have a getADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.getADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getADashboardList(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getADashboardList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateADashboardList - errors', () => {
      it('should have a updateADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.updateADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateADashboardList(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateADashboardList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateADashboardList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteADashboardList - errors', () => {
      it('should have a deleteADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteADashboardList(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteADashboardList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDashboardLists - errors', () => {
      it('should have a getAllDashboardLists function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDashboardLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllDashboardLists(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllDashboardLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllDashboardLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllDashboardLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createADashboardList - errors', () => {
      it('should have a createADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.createADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createADashboardList(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createADashboardList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createADashboardList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItemsOfADashboardList - errors', () => {
      it('should have a getItemsOfADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.getItemsOfADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getItemsOfADashboardList(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getItemsOfADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getItemsOfADashboardList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getItemsOfADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addItemsToDashboardList - errors', () => {
      it('should have a addItemsToDashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.addItemsToDashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addItemsToDashboardList(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addItemsToDashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.addItemsToDashboardList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addItemsToDashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.addItemsToDashboardList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addItemsToDashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateItemsOfADashboardList - errors', () => {
      it('should have a updateItemsOfADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.updateItemsOfADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateItemsOfADashboardList(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateItemsOfADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateItemsOfADashboardList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateItemsOfADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateItemsOfADashboardList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateItemsOfADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteItemsFromADashboardList - errors', () => {
      it('should have a deleteItemsFromADashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteItemsFromADashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteItemsFromADashboardList(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteItemsFromADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteItemsFromADashboardList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteItemsFromADashboardList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleMonitorDowntime - errors', () => {
      it('should have a scheduleMonitorDowntime function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleMonitorDowntime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.scheduleMonitorDowntime(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-scheduleMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.scheduleMonitorDowntime('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-scheduleMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.scheduleMonitorDowntime('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-scheduleMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMointorDowntimes - errors', () => {
      it('should have a getAllMointorDowntimes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMointorDowntimes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllMointorDowntimes(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllMointorDowntimes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllMointorDowntimes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllMointorDowntimes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitorDowntime - errors', () => {
      it('should have a updateMonitorDowntime function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitorDowntime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMonitorDowntime(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateMonitorDowntime('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateMonitorDowntime('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelMonitorDowntime - errors', () => {
      it('should have a cancelMonitorDowntime function', (done) => {
        try {
          assert.equal(true, typeof a.cancelMonitorDowntime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.cancelMonitorDowntime(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-cancelMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.cancelMonitorDowntime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-cancelMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMonitorDowntime - errors', () => {
      it('should have a getAMonitorDowntime function', (done) => {
        try {
          assert.equal(true, typeof a.getAMonitorDowntime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAMonitorDowntime(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAMonitorDowntime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAMonitorDowntime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelMonitorDowntimesByScope - errors', () => {
      it('should have a cancelMonitorDowntimesByScope function', (done) => {
        try {
          assert.equal(true, typeof a.cancelMonitorDowntimesByScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.cancelMonitorDowntimesByScope(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-cancelMonitorDowntimesByScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.cancelMonitorDowntimesByScope('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-cancelMonitorDowntimesByScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.cancelMonitorDowntimesByScope('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-cancelMonitorDowntimesByScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllEmbeddableGraphs - errors', () => {
      it('should have a getAllEmbeddableGraphs function', (done) => {
        try {
          assert.equal(true, typeof a.getAllEmbeddableGraphs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllEmbeddableGraphs(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllEmbeddableGraphs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllEmbeddableGraphs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllEmbeddableGraphs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEmbedGraph - errors', () => {
      it('should have a createEmbedGraph function', (done) => {
        try {
          assert.equal(true, typeof a.createEmbedGraph === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createEmbedGraph(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createEmbedGraph', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createEmbedGraph('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createEmbedGraph', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createEmbedGraph('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createEmbedGraph', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASpecificEmbed - errors', () => {
      it('should have a getASpecificEmbed function', (done) => {
        try {
          assert.equal(true, typeof a.getASpecificEmbed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getASpecificEmbed(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getASpecificEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getASpecificEmbed('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getASpecificEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableEmbed - errors', () => {
      it('should have a enableEmbed function', (done) => {
        try {
          assert.equal(true, typeof a.enableEmbed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.enableEmbed(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-enableEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.enableEmbed('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-enableEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeEmbed - errors', () => {
      it('should have a revokeEmbed function', (done) => {
        try {
          assert.equal(true, typeof a.revokeEmbed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.revokeEmbed(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-revokeEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.revokeEmbed('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-revokeEmbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAnEvent - errors', () => {
      it('should have a postAnEvent function', (done) => {
        try {
          assert.equal(true, typeof a.postAnEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAnEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.postAnEvent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.postAnEvent('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-postAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryTheEventStream - errors', () => {
      it('should have a queryTheEventStream function', (done) => {
        try {
          assert.equal(true, typeof a.queryTheEventStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.queryTheEventStream(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTheEventStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.queryTheEventStream('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTheEventStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnEvent - errors', () => {
      it('should have a getAnEvent function', (done) => {
        try {
          assert.equal(true, typeof a.getAnEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAnEvent(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAnEvent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnEvent - errors', () => {
      it('should have a deleteAnEvent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAnEvent(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAnEvent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#graphASnapshot - errors', () => {
      it('should have a graphASnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.graphASnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.graphASnapshot(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.graphASnapshot('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metricQuery', (done) => {
        try {
          a.graphASnapshot('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'metricQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing start', (done) => {
        try {
          a.graphASnapshot('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'start is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing end', (done) => {
        try {
          a.graphASnapshot('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'end is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing title', (done) => {
        try {
          a.graphASnapshot('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'title is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-graphASnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchHosts - errors', () => {
      it('should have a searchHosts function', (done) => {
        try {
          assert.equal(true, typeof a.searchHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.searchHosts(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.searchHosts('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.searchHosts('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sortField', (done) => {
        try {
          a.searchHosts('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sortField is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sortDir', (done) => {
        try {
          a.searchHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sortDir is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing start', (done) => {
        try {
          a.searchHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'start is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.searchHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostTotals - errors', () => {
      it('should have a hostTotals function', (done) => {
        try {
          assert.equal(true, typeof a.hostTotals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.hostTotals(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-hostTotals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.hostTotals('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-hostTotals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#muteAHost - errors', () => {
      it('should have a muteAHost function', (done) => {
        try {
          assert.equal(true, typeof a.muteAHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.muteAHost(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.muteAHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.muteAHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unmuteAHost - errors', () => {
      it('should have a unmuteAHost function', (done) => {
        try {
          assert.equal(true, typeof a.unmuteAHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.unmuteAHost(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.unmuteAHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.unmuteAHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAWSAccounts - errors', () => {
      it('should have a listAWSAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.listAWSAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.listAWSAccounts(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAWSAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.listAWSAccounts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAWSAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnAWSIntegration - errors', () => {
      it('should have a createAnAWSIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.createAnAWSIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAnAWSIntegration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAnAWSIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createAnAWSIntegration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAnAWSIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createAnAWSIntegration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAnAWSIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAWSIntegration - errors', () => {
      it('should have a deleteAnAWSIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAWSIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAnAWSIntegration(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAWSIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAnAWSIntegration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAWSIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAvailableNamespaceRules - errors', () => {
      it('should have a listAvailableNamespaceRules function', (done) => {
        try {
          assert.equal(true, typeof a.listAvailableNamespaceRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.listAvailableNamespaceRules(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAvailableNamespaceRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.listAvailableNamespaceRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAvailableNamespaceRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAWSFilteringRules - errors', () => {
      it('should have a listAWSFilteringRules function', (done) => {
        try {
          assert.equal(true, typeof a.listAWSFilteringRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.listAWSFilteringRules(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAWSFilteringRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.listAWSFilteringRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAWSFilteringRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configureAWSFilteringRuleCopy - errors', () => {
      it('should have a configureAWSFilteringRuleCopy function', (done) => {
        try {
          assert.equal(true, typeof a.configureAWSFilteringRuleCopy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.configureAWSFilteringRuleCopy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-configureAWSFilteringRuleCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.configureAWSFilteringRuleCopy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-configureAWSFilteringRuleCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.configureAWSFilteringRuleCopy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-configureAWSFilteringRuleCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAWSFilteringRule - errors', () => {
      it('should have a deleteAWSFilteringRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAWSFilteringRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAWSFilteringRule(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAWSFilteringRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAWSFilteringRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAWSFilteringRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateNewExternalIDs - errors', () => {
      it('should have a generateNewExternalIDs function', (done) => {
        try {
          assert.equal(true, typeof a.generateNewExternalIDs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.generateNewExternalIDs(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-generateNewExternalIDs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.generateNewExternalIDs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-generateNewExternalIDs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.generateNewExternalIDs('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-generateNewExternalIDs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAzureAccount - errors', () => {
      it('should have a listAzureAccount function', (done) => {
        try {
          assert.equal(true, typeof a.listAzureAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.listAzureAccount(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAzureAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.listAzureAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listAzureAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAzureAccountSHostFilters - errors', () => {
      it('should have a updateAzureAccountSHostFilters function', (done) => {
        try {
          assert.equal(true, typeof a.updateAzureAccountSHostFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAzureAccountSHostFilters(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAzureAccountSHostFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateAzureAccountSHostFilters('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAzureAccountSHostFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateAzureAccountSHostFilters('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAzureAccountSHostFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAzureAccount - errors', () => {
      it('should have a deleteAnAzureAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAzureAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAnAzureAccount(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAzureAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAnAzureAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAzureAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGCPAccounts - errors', () => {
      it('should have a listGCPAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.listGCPAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.listGCPAccounts(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listGCPAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.listGCPAccounts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-listGCPAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGCPAccount - errors', () => {
      it('should have a createGCPAccount function', (done) => {
        try {
          assert.equal(true, typeof a.createGCPAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGCPAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createGCPAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createGCPAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createGCPAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createGCPAccount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createGCPAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAGCPServiceAccountSAutomuteOption - errors', () => {
      it('should have a updateAGCPServiceAccountSAutomuteOption function', (done) => {
        try {
          assert.equal(true, typeof a.updateAGCPServiceAccountSAutomuteOption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAGCPServiceAccountSAutomuteOption(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAGCPServiceAccountSAutomuteOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateAGCPServiceAccountSAutomuteOption('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAGCPServiceAccountSAutomuteOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateAGCPServiceAccountSAutomuteOption('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateAGCPServiceAccountSAutomuteOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAGCPServiceAccount - errors', () => {
      it('should have a deleteAGCPServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAGCPServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAGCPServiceAccount(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAGCPServiceAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAGCPServiceAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAGCPServiceAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPagerDutyConfiguration - errors', () => {
      it('should have a getPagerDutyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getPagerDutyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getPagerDutyConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getPagerDutyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getPagerDutyConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getPagerDutyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNewServicesAndSchedules - errors', () => {
      it('should have a addNewServicesAndSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.addNewServicesAndSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addNewServicesAndSchedules(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addNewServicesAndSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.addNewServicesAndSchedules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addNewServicesAndSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.addNewServicesAndSchedules('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addNewServicesAndSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePagerDutyConfiguration - errors', () => {
      it('should have a deletePagerDutyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deletePagerDutyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deletePagerDutyConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deletePagerDutyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deletePagerDutyConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deletePagerDutyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlackConfigurationDetails - errors', () => {
      it('should have a getSlackConfigurationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getSlackConfigurationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getSlackConfigurationDetails(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getSlackConfigurationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getSlackConfigurationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getSlackConfigurationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addChannelsToExistingConfiguration - errors', () => {
      it('should have a addChannelsToExistingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.addChannelsToExistingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addChannelsToExistingConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addChannelsToExistingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.addChannelsToExistingConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addChannelsToExistingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.addChannelsToExistingConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addChannelsToExistingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSlackConfigurationDetails - errors', () => {
      it('should have a deleteSlackConfigurationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSlackConfigurationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteSlackConfigurationDetails(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteSlackConfigurationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteSlackConfigurationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteSlackConfigurationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWebhookConfigurations - errors', () => {
      it('should have a getAllWebhookConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllWebhookConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllWebhookConfigurations(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllWebhookConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllWebhookConfigurations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllWebhookConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addToCurrentConfiguration - errors', () => {
      it('should have a addToCurrentConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.addToCurrentConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addToCurrentConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addToCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.addToCurrentConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addToCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.addToCurrentConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addToCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebhookConfigurations - errors', () => {
      it('should have a deleteWebhookConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebhookConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteWebhookConfigurations(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteWebhookConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteWebhookConfigurations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteWebhookConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCurrentConfiguration - errors', () => {
      it('should have a replaceCurrentConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCurrentConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCurrentConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replaceCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.replaceCurrentConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replaceCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.replaceCurrentConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-replaceCurrentConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAPIKey - errors', () => {
      it('should have a getAllAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllAPIKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllAPIKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createANewAPIKey - errors', () => {
      it('should have a createANewAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.createANewAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createANewAPIKey(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createANewAPIKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createANewAPIKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAGivenAPIKey - errors', () => {
      it('should have a getAGivenAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.getAGivenAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAGivenAPIKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAGivenAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAGivenAPIKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAGivenAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAGivenAPIKey - errors', () => {
      it('should have a editAGivenAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.editAGivenAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAGivenAPIKey(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editAGivenAPIKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editAGivenAPIKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAPIKey - errors', () => {
      it('should have a deleteAnAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAnAPIKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAnAPIKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnAPIKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationKey - errors', () => {
      it('should have a getAllApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllApplicationKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllApplicationKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createANewApplicationKey - errors', () => {
      it('should have a createANewApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.createANewApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createANewApplicationKey(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createANewApplicationKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createANewApplicationKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createANewApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAGivenApplicationKey - errors', () => {
      it('should have a getAGivenApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.getAGivenApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAGivenApplicationKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAGivenApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAGivenApplicationKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAGivenApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAGivenApplicationKey - errors', () => {
      it('should have a editAGivenApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.editAGivenApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAGivenApplicationKey(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editAGivenApplicationKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editAGivenApplicationKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAGivenApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnApplicationKey - errors', () => {
      it('should have a deleteAnApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAnApplicationKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAnApplicationKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAnApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendLogsOverHTTP - errors', () => {
      it('should have a sendLogsOverHTTP function', (done) => {
        try {
          assert.equal(true, typeof a.sendLogsOverHTTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.sendLogsOverHTTP(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendLogsOverHTTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddsource', (done) => {
        try {
          a.sendLogsOverHTTP('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ddsource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendLogsOverHTTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.sendLogsOverHTTP('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendLogsOverHTTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostname', (done) => {
        try {
          a.sendLogsOverHTTP('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'hostname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendLogsOverHTTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAListOfLogs - errors', () => {
      it('should have a getAListOfLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getAListOfLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAListOfLogs(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAListOfLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAListOfLogs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAListOfLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAListOfLogs('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAListOfLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllActiveMetrics - errors', () => {
      it('should have a getAllActiveMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getAllActiveMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllActiveMetrics(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllActiveMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllActiveMetrics('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllActiveMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.getAllActiveMetrics('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllActiveMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.getAllActiveMetrics('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllActiveMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendTimeSeriesPoint - errors', () => {
      it('should have a sendTimeSeriesPoint function', (done) => {
        try {
          assert.equal(true, typeof a.sendTimeSeriesPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.sendTimeSeriesPoint(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendTimeSeriesPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.sendTimeSeriesPoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendTimeSeriesPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.sendTimeSeriesPoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendTimeSeriesPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryTimeSeriesPoints - errors', () => {
      it('should have a queryTimeSeriesPoints function', (done) => {
        try {
          assert.equal(true, typeof a.queryTimeSeriesPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.queryTimeSeriesPoints(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTimeSeriesPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.queryTimeSeriesPoints('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTimeSeriesPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.queryTimeSeriesPoints('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTimeSeriesPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing to', (done) => {
        try {
          a.queryTimeSeriesPoints('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'to is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTimeSeriesPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.queryTimeSeriesPoints('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-queryTimeSeriesPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#viewMetricMetadata - errors', () => {
      it('should have a viewMetricMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.viewMetricMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.viewMetricMetadata(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-viewMetricMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.viewMetricMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-viewMetricMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editMetricMetadata - errors', () => {
      it('should have a editMetricMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.editMetricMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editMetricMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editMetricMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editMetricMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editMetricMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editMetricMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editMetricMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchMetrics - errors', () => {
      it('should have a searchMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.searchMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing q', (done) => {
        try {
          a.searchMetrics(null, null, null, (data, error) => {
            try {
              const displayE = 'q is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.searchMetrics('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.searchMetrics('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-searchMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAMonitor - errors', () => {
      it('should have a createAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.createAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createAMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createAMonitor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMonitorSDetails - errors', () => {
      it('should have a getAMonitorSDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getAMonitorSDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAMonitorSDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAMonitorSDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupStates', (done) => {
        try {
          a.getAMonitorSDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupStates is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAMonitorSDetails - errors', () => {
      it('should have a editAMonitorSDetails function', (done) => {
        try {
          assert.equal(true, typeof a.editAMonitorSDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAMonitorSDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editAMonitorSDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editAMonitorSDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editAMonitorSDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAMonitor - errors', () => {
      it('should have a deleteAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteAMonitor(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteAMonitor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resolveMonitor - errors', () => {
      it('should have a resolveMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.resolveMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.resolveMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-resolveMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.resolveMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-resolveMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.resolveMonitor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-resolveMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#muteASpecificMonitor - errors', () => {
      it('should have a muteASpecificMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.muteASpecificMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.muteASpecificMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.muteASpecificMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.muteASpecificMonitor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unmuteASpecificMonitor - errors', () => {
      it('should have a unmuteASpecificMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.unmuteASpecificMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.unmuteASpecificMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.unmuteASpecificMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.unmuteASpecificMonitor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteASpecificMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#muteAllMonitors - errors', () => {
      it('should have a muteAllMonitors function', (done) => {
        try {
          assert.equal(true, typeof a.muteAllMonitors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.muteAllMonitors(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.muteAllMonitors('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.muteAllMonitors('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-muteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unmuteAllMonitors - errors', () => {
      it('should have a unmuteAllMonitors function', (done) => {
        try {
          assert.equal(true, typeof a.unmuteAllMonitors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.unmuteAllMonitors(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.unmuteAllMonitors('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.unmuteAllMonitors('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-unmuteAllMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateAMonitorDefinitions - errors', () => {
      it('should have a validateAMonitorDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.validateAMonitorDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.validateAMonitorDefinitions(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-validateAMonitorDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.validateAMonitorDefinitions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-validateAMonitorDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.validateAMonitorDefinitions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-validateAMonitorDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitorSearch - errors', () => {
      it('should have a monitorSearch function', (done) => {
        try {
          assert.equal(true, typeof a.monitorSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.monitorSearch(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.monitorSearch('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.monitorSearch('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing page', (done) => {
        try {
          a.monitorSearch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'page is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing perPage', (done) => {
        try {
          a.monitorSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'perPage is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.monitorSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitorGroupSearch - errors', () => {
      it('should have a monitorGroupSearch function', (done) => {
        try {
          assert.equal(true, typeof a.monitorGroupSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.monitorGroupSearch(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.monitorGroupSearch('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.monitorGroupSearch('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing page', (done) => {
        try {
          a.monitorGroupSearch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'page is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing perPage', (done) => {
        try {
          a.monitorGroupSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'perPage is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.monitorGroupSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-monitorGroupSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createChildOrganization - errors', () => {
      it('should have a createChildOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.createChildOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createChildOrganization(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createChildOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createChildOrganization('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createChildOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createChildOrganization('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createChildOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOrganizations - errors', () => {
      it('should have a getAllOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllOrganizations(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllOrganizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllOrganizations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllOrganizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganization - errors', () => {
      it('should have a getOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getOrganization(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganization - errors', () => {
      it('should have a updateOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateOrganization(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateOrganization('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateOrganization('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadIDPMetadata - errors', () => {
      it('should have a uploadIDPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.uploadIDPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.uploadIDPMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-uploadIDPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.uploadIDPMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-uploadIDPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.uploadIDPMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-uploadIDPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createATest - errors', () => {
      it('should have a createATest function', (done) => {
        try {
          assert.equal(true, typeof a.createATest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createATest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.createATest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.createATest('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-createATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTests - errors', () => {
      it('should have a getAllTests function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllTests(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllTests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllTests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllTests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startOrPauseATest - errors', () => {
      it('should have a startOrPauseATest function', (done) => {
        try {
          assert.equal(true, typeof a.startOrPauseATest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startOrPauseATest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-startOrPauseATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.startOrPauseATest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-startOrPauseATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.startOrPauseATest('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-startOrPauseATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editATest - errors', () => {
      it('should have a editATest function', (done) => {
        try {
          assert.equal(true, typeof a.editATest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editATest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.editATest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.editATest('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-editATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteATest - errors', () => {
      it('should have a deleteATest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteATest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteATest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteATest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.deleteATest('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-deleteATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTestResults - errors', () => {
      it('should have a getAllTestResults function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTestResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAllTestResults(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllTestResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAllTestResults('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAllTestResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASpecificTestResult - errors', () => {
      it('should have a getASpecificTestResult function', (done) => {
        try {
          assert.equal(true, typeof a.getASpecificTestResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getASpecificTestResult(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getASpecificTestResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getASpecificTestResult('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getASpecificTestResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getATest - errors', () => {
      it('should have a getATest function', (done) => {
        try {
          assert.equal(true, typeof a.getATest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getATest(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getATest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getATest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesForBrowserChecks - errors', () => {
      it('should have a getDevicesForBrowserChecks function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesForBrowserChecks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getDevicesForBrowserChecks(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getDevicesForBrowserChecks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getDevicesForBrowserChecks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getDevicesForBrowserChecks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableLocations - errors', () => {
      it('should have a getAvailableLocations function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailableLocations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getAvailableLocations(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAvailableLocations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getAvailableLocations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getAvailableLocations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTags - errors', () => {
      it('should have a getTags function', (done) => {
        try {
          assert.equal(true, typeof a.getTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getTags(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostTags - errors', () => {
      it('should have a getHostTags function', (done) => {
        try {
          assert.equal(true, typeof a.getHostTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getHostTags(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getHostTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTagsToAHost - errors', () => {
      it('should have a addTagsToAHost function', (done) => {
        try {
          assert.equal(true, typeof a.addTagsToAHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTagsToAHost(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addTagsToAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.addTagsToAHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addTagsToAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.addTagsToAHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-addTagsToAHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostTags - errors', () => {
      it('should have a updateHostTags function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostTags(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateHostTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.updateHostTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-updateHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeHostTags - errors', () => {
      it('should have a removeHostTags function', (done) => {
        try {
          assert.equal(true, typeof a.removeHostTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.removeHostTags(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-removeHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.removeHostTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-removeHostTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendTraces - errors', () => {
      it('should have a sendTraces function', (done) => {
        try {
          assert.equal(true, typeof a.sendTraces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.sendTraces(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendTraces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendServices - errors', () => {
      it('should have a sendServices function', (done) => {
        try {
          assert.equal(true, typeof a.sendServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.sendServices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-sendServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostsAndContainersHourlyUsage - errors', () => {
      it('should have a getHostsAndContainersHourlyUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getHostsAndContainersHourlyUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getHostsAndContainersHourlyUsage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostsAndContainersHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getHostsAndContainersHourlyUsage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostsAndContainersHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHr', (done) => {
        try {
          a.getHostsAndContainersHourlyUsage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostsAndContainersHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endHr', (done) => {
        try {
          a.getHostsAndContainersHourlyUsage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getHostsAndContainersHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogsHourlyUsage - errors', () => {
      it('should have a getLogsHourlyUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getLogsHourlyUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getLogsHourlyUsage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getLogsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getLogsHourlyUsage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getLogsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHr', (done) => {
        try {
          a.getLogsHourlyUsage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getLogsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endHr', (done) => {
        try {
          a.getLogsHourlyUsage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getLogsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomMetricsHourlyUsage - errors', () => {
      it('should have a getCustomMetricsHourlyUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomMetricsHourlyUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getCustomMetricsHourlyUsage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getCustomMetricsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getCustomMetricsHourlyUsage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getCustomMetricsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHr', (done) => {
        try {
          a.getCustomMetricsHourlyUsage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getCustomMetricsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endHr', (done) => {
        try {
          a.getCustomMetricsHourlyUsage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getCustomMetricsHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTop500CustomMetricsByHourlyAverage - errors', () => {
      it('should have a getTop500CustomMetricsByHourlyAverage function', (done) => {
        try {
          assert.equal(true, typeof a.getTop500CustomMetricsByHourlyAverage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getTop500CustomMetricsByHourlyAverage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTop500CustomMetricsByHourlyAverage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getTop500CustomMetricsByHourlyAverage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTop500CustomMetricsByHourlyAverage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing month', (done) => {
        try {
          a.getTop500CustomMetricsByHourlyAverage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'month is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTop500CustomMetricsByHourlyAverage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing names', (done) => {
        try {
          a.getTop500CustomMetricsByHourlyAverage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'names is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTop500CustomMetricsByHourlyAverage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultiOrgUsageDetails - errors', () => {
      it('should have a getMultiOrgUsageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getMultiOrgUsageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getMultiOrgUsageDetails(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getMultiOrgUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getMultiOrgUsageDetails('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getMultiOrgUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startMonth', (done) => {
        try {
          a.getMultiOrgUsageDetails('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startMonth is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getMultiOrgUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endMonth', (done) => {
        try {
          a.getMultiOrgUsageDetails('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endMonth is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getMultiOrgUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing includeOrgIds', (done) => {
        try {
          a.getMultiOrgUsageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'includeOrgIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getMultiOrgUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceSearchHourlyUsage - errors', () => {
      it('should have a getTraceSearchHourlyUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceSearchHourlyUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getTraceSearchHourlyUsage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTraceSearchHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getTraceSearchHourlyUsage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTraceSearchHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHr', (done) => {
        try {
          a.getTraceSearchHourlyUsage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTraceSearchHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endHr', (done) => {
        try {
          a.getTraceSearchHourlyUsage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getTraceSearchHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFargateHourlyUsage - errors', () => {
      it('should have a getFargateHourlyUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getFargateHourlyUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.getFargateHourlyUsage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getFargateHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getFargateHourlyUsage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getFargateHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHr', (done) => {
        try {
          a.getFargateHourlyUsage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getFargateHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endHr', (done) => {
        try {
          a.getFargateHourlyUsage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endHr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-datadog-adapter-getFargateHourlyUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
